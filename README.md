# arduino-fqbn-generator

Je skript v *Tcl* na generovanie **FQBN** a.k.a. **Fully Qualified Board Name** na kompilovanie nahrávanie programov do vývojových dosiek *Arduino*, *ESPxx*, *Raspberry Pi Pico* atď.


## Potrebnosti

Potrebné nástroje:

`tcl`

Inštalácia v debian prostredí: `sudo apt install tcl`.

## Spustenie

`tclsh arduino-fbqn-generator.tcl`

Spustenie vytvorí súbor **fqbn.txt**, v ktorom je *FQBN* pre konkrétnu inštalovanú dosku a tiež súbor **options.txt**, v ktorom je zoznam ďalších upresnení, ktoré, ak sú požadované, je potrebné dopísať do **fqbn.txt**, alebo do **Makefile**. 

Príklad *FQBN* pre *Raspberry Pi Pico*:

```
rp2040:rp2040:rpipico:flash=2097152_0,freq=133,opt=Small,rtti=Disabled,stackprotect=Disabled,exceptions=Disabled,dbgport=Disabled,dbglvl=None,usbstack=picosdk,ipstack=ipv4only
```	


## Poznámka

V podadresári **makefile_for_arduino-cli** je **Makefile** pre používanie v *arduino-cli*. Tento Makefile číta *FQBN* zo spomínaného súboru **fqbn.txt**

Prvé dva riadky kódu obsahujú premenné:
```
set arduinoCliBin "/home/richard/.arduino15/arduino-cli"
set arduino15Path "/home/richard/.arduino15/"
```

Ako ich pomenovanie indikuje, jedná sa o cesty k inštalácii binárky `arduino-cli` a tiež cesty, kde sú ukladané definície dosiek a podporné programy (sú o.i. zadané i v súbore `arduino-cli.yaml` - `directories: data: ~/.arduino15`.