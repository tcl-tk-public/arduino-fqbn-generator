set arduinoCliBin "/home/richard/.arduino15/arduino-cli"
set arduino15Path "/home/richard/.arduino15/"

puts "-----------------------------------------"
puts "Jednoduchý generátor FGBN pre arduino-cli"
puts "-----------------------------------------\n"


puts "\033\[34mZoznam inštalovaných dosiek:\033\[0m"
append exString "ls " $arduino15Path "packages/ | grep -v builtin"
set boards [exec {*}$exString]
set boardList [split $boards "\n"]
set index 0
foreach item $boardList {
    puts "$index → $item"
    incr index
}

puts "\033\[31m\n➲ Zadaj číslo dosky:\033\[0m"
gets stdin nrBoard
set fqbn_b [lindex $boardList $nrBoard]


puts "\033\[34m\nZoznam variantov dosiek $fqbn_b:\033\[0m "

set exString ""
append exString $arduinoCliBin " board listall | grep " $fqbn_b " > variants.txt"
exec {*}$exString

set file [open "variants.txt" r]
set data [read $file]
close $file
set variantsList [split $data "\n"]
set variantsList [lrange $variantsList 0 end-1]
set index 0
foreach item $variantsList {
    puts "$index → $item"
    incr index
}

puts "\033\[31m\n➲ Zadaj číslo varianty dosky:\033\[0m"
gets stdin nrVariant
set fqbn_v [split [lindex $variantsList $nrVariant] " "]
set fqbn_bv [lindex $fqbn_v end]


puts "\033\[34m\nZoznam procesorov pre variant dosiek $fqbn_bv:\033\[0m "

set exString ""
set fqbn_comp ""

append exString $arduinoCliBin " board details -b " $fqbn_bv " | grep cpu= > cpu.txt"

if {[catch {exec {*}$exString}]} {
    puts "\033\[34m\nDoska nemá ďalšie parametre pre cpu\033\[0m"
    set fqbn_comp $fqbn_bv
    
} else {
    puts "\033\[34mZoznam parametrov cpu:\033\[0m"
    set file [open "cpu.txt" r]
    set data [read $file]
    close $file
    set cpuList [split $data "\n"]
    set cpuList [lrange $cpuList 0 end-1]
    set index 0
    foreach item $cpuList {
	puts "$index → $item"
	incr index
    }
    puts "\033\[31m\n➲ Zadaj číslo parametra cpu:\033\[0m"
    gets stdin nrCPU
    set fqbn_c [split [lindex $cpuList $nrCPU] " "]
    set fqbn_cv [lindex [split $fqbn_c " "] end]
    append fqbn_comp $fqbn_bv ":" $fqbn_cv
}


puts "\n-------------------------------------------------------------------------"
puts "ℹ Generovaný FQBN: $fqbn_comp v súbore 'fqbn.txt'"
puts "-------------------------------------------------------------------------"

set file [open "fqbn.txt" "w"]
puts $file $fqbn_comp
close $file
exec {*}"rm variants.txt"
exec {*}"rm cpu.txt"

puts "\nℹ Ostatné parametre (vhodné napr. pre RP2040) ukladám do súboru 'options.txt'"
set exStr ""
append exStr $arduinoCliBin " board details -b " $fqbn_bv " > options.txt"
exec {*}$exStr
